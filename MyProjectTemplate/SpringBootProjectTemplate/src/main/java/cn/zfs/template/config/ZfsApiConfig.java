package cn.zfs.template.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @ClassName: ZfsApiConfig
 * @Author: ZFS
 * @Date: 2018/10/19 17:50
 */
@Slf4j
@Configuration
@ComponentScan("cn.zhangfusheng.api.rest")
public class ZfsApiConfig implements CommandLineRunner {

    @Override
    public void run(String... args) {
        log.info("初始化zfs-zpi----start");
        cn.zhangfusheng.api.config.ZfsApiConfig.initZfsApiConfig("cn.zfs.template.controller");
        HashMap<String, String> groupPackageMap = new HashMap<>();
        groupPackageMap.put("one","cn.zfs.template.controller");
        cn.zhangfusheng.api.config.ZfsApiConfig.initZfsApiConfig(groupPackageMap);
        log.info("初始化zfs-zpi----end");
    }
}
