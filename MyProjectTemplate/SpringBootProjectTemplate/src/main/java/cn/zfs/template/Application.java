package cn.zfs.template;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName: Application
 * @Author: ZFS
 * @Date: 2018/11/26 12:08
 */
@Slf4j
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        log.info("start spring boot template");
        SpringApplication.run(Application.class, args);
        log.info("end spring boot template");
    }

}
