package cn.zfs.template.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Slf4j
@EnableSwagger2
@Configuration
@ComponentScan("cn.zfs")
public class SwaggerConfig extends WebMvcConfigurerAdapter {

    /**
     * 如果你在配置文件中配置了页面访问的相关信息,就需要有该方法,否则无法访问swagger-ui.html
     *
     * @param registry --
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("初始化swagger页面的位置---start");
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        log.info("初始化swagger页面的位置---end");
    }

    @Bean
    public Docket createRestApi() {
        log.info("初始化swagger-api文档----start");
        ArrayList<SecurityScheme> securitySchemes = new ArrayList<>();
        securitySchemes.add(new ApiKey("Set-Cookie", "Cookie", "header"));
        securitySchemes.add(new ApiKey("Authorization", "Authorization", "header"));
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.zfs"))
                .paths(PathSelectors.any())
                .build().securitySchemes(securitySchemes);
        log.info("初始化swagger-api文档----end");
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot 项目模板") //设置文档的标题
                .description("Spring boot template") //设置文档的描述->1.Overview
                .termsOfServiceUrl("Spring boot template") //设置文档的License信息->1.3 License information
                .version("1.0")
                .build();
    }

}
