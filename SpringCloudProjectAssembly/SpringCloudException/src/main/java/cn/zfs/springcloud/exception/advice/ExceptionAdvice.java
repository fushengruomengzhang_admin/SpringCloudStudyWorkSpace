package cn.zfs.springcloud.exception.advice;

import cn.zfs.springcloud.exception.global.GlobalException;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 自定义异常拦截器
 *
 * @author Administrator
 */
@SuppressWarnings("Duplicates")
@ControllerAdvice
public class ExceptionAdvice {

    private Logger logger = LoggerFactory.getLogger(ExceptionAdvice.class);

    /**
     * 全局异常捕捉处理
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = {GlobalException.class, Exception.class})
    public BaseResponse errorHandler(Exception e) {
        if (e instanceof GlobalException) {
            logger.info(e.getMessage());
            return BaseResponse.fail(e.getMessage());
        } else {
            logger.error(e.getMessage(), e);
            return BaseResponse.fail("系统出现异常,请联系管理员");
        }
    }
}
