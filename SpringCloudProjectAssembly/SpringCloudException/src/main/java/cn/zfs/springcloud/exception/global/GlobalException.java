package cn.zfs.springcloud.exception.global;

public class GlobalException extends Exception {

    private String message;

    public GlobalException(String message, Object... objects) {
        if (objects.length > 0 && message.contains("{}")) {
            for (Object obj : objects) {
                message = message.replaceFirst("\\{\\}", obj == null ? "null" : String.valueOf(obj));
            }
        }
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
