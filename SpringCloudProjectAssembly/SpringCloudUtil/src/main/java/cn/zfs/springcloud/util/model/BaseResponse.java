package cn.zfs.springcloud.util.model;


import cn.zfs.springcloud.util.enums.BaseResponseStatusEnum;
import cn.zfs.springcloud.util.util.StringUtil;

/**
 * Controller层返回值封装
 *
 * @author Administrator
 */
@SuppressWarnings("ALL")
public class BaseResponse<T> {

    // 消息
    private String msg;
    // 响应数据
    private T data;
    // 响应状态
    private int result;

    private BaseResponse() {
    }

    public BaseResponse(String msg, T data, int result) {
        this.msg = msg;
        this.data = data;
        this.result = result;
    }

    /**
     * 返回成功的消息
     *
     * @param msg
     * @param objects
     * @return
     */
    public static BaseResponse success(String msg, Object... objects) {
        return getBaserResponse(msg, null, BaseResponseStatusEnum.SUCCESS.getStatus(), objects);
    }

    /**
     * 返回成功的数据
     * @param data
     * @return
     */
    public static BaseResponse success(Object data) {
        return getBaserResponse(null, data, BaseResponseStatusEnum.SUCCESS.getStatus());
    }

    /**
     * 返回失败的消息
     *
     * @param msg
     * @param objects
     * @return
     */
    public static BaseResponse fail(String msg, Object... objects) {
        return getBaserResponse(msg, null, BaseResponseStatusEnum.FAIL.getStatus(), objects);
    }

    public static BaseResponse getBaserResponse() {
        return new BaseResponse();
    }

    private static BaseResponse getBaserResponse(String msg, Object data, int result, Object... objects) {
        msg = StringUtil.formatString(msg, "{}", "\\{\\}", objects);
        return new BaseResponse(msg, data, result);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
