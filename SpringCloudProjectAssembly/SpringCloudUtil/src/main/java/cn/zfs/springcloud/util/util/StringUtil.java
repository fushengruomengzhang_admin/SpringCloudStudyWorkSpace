package cn.zfs.springcloud.util.util;

/**
 * @author Administrator
 */
public class StringUtil {

    public static String formatString(String str, String replaceStr, String regex, Object... objects) {
        if (replaceStr == null || regex == null) {
            return str;
        }
        if (objects.length > 0 && str.contains(replaceStr)) {
            for (Object obj : objects) {
                str = str.replaceFirst(regex, obj == null ? "null" : String.valueOf(obj));
            }
        }
        return str;
    }
}
