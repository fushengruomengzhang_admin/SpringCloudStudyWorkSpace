package cn.zfs.springcloud.util.model;

import cn.zfs.springcloud.util.util.IntegerUtil;

/**
 * @author Administrator
 */
public class BasePageList<T> {

    private static final int DEFAULT_PAGESIZE = 10;

    private Integer pageSize; // 每页展示大小
    private Integer pageNum; // 当前页数
    private Integer totalPages; // 总页数
    private Integer totalNum; // 总数据条数
    private Integer start;
    private T data;

    public BasePageList() {
    }

    public BasePageList(Integer pageSize, Integer pageNum) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }

    public BasePageList(int pageSize, int pageNum) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return IntegerUtil.isNullOrZero(pageSize) ? DEFAULT_PAGESIZE : pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return IntegerUtil.isNullOrZero(pageNum) ? 1 : pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getTotalPages() {
        return totalNum % this.getPageSize() == 0 ? totalNum / this.getPageSize() : totalNum / this.getPageSize() + 1;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getStart() {
        if(pageNum > 1){
            start = (pageNum - 1) * this.getPageSize();
        }else{
            start = 0;
        }
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
