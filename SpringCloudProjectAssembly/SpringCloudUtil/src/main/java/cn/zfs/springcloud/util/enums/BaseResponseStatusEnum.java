package cn.zfs.springcloud.util.enums;

/**
 * @author Administrator
 */
public enum BaseResponseStatusEnum {

    /**
     * 100 失败 200成功
     */
    SUCCESS(200, "成功"),
    FAIL(100, "失败");

    private int status;
    private String name;

    BaseResponseStatusEnum(int status, String name) {
        this.status = status;
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
