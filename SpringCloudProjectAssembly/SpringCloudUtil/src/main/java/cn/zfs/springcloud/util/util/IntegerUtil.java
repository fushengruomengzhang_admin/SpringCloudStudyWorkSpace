package cn.zfs.springcloud.util.util;

public class IntegerUtil {

    /**
     * 判断一个integer类型数据是否为null或者为0
     *
     * @param integer
     * @return
     */
    public static boolean isNullOrZero(Integer integer) {
        return integer == null || integer == 0;
    }

    /**
     * 判断多个integer类型的数组中,如果有一个为null或者为0返回true
     * @param integers
     * @return
     */
    public static boolean isNullOrZero(Integer... integers) {
        if (integers == null) {
            return true;
        } else {
            for (Integer integer : integers) {
                if (isNullOrZero(integer)) {
                    return true;
                }
            }
        }
        return false;
    }
}
