package cn.zfs.springcloud.util.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * @ClassName: HttpClientUtils
 * @Author: ZFS
 * @Date: 2018/10/12 13:26
 */
@SuppressWarnings("Duplicates")
public class HttpClientUtils {

    public static final String POST = "POST";
    public static final String GET = "GET";


    /**
     * swagger 基础API访问
     *
     * @return string
     * @throws IOException IOException
     */
    public static String requestUrl(
            String url, String method, String domain, Cookie[] cookies, Map<String, String> headerMap, byte[] params) throws IOException {
        HttpClientBuilder httpClientBuilder = HttpClients.custom();
        // 设置Cookie
        CookieStore cookieStore = setCookies(cookies, domain);
        if (cookieStore != null) {
            httpClientBuilder = httpClientBuilder.setDefaultCookieStore(cookieStore);
        }
        CloseableHttpClient httpClient = httpClientBuilder.build();
        // 根据请求方式生成请求对象
        HttpRequestBase httpRequestBase = getHttpRequestBase(method, params);
        //设置url
        httpRequestBase.setURI(URI.create(url));
        // 设置header
        httpRequestBase = setHeader(headerMap, httpRequestBase);
        // 执行
        HttpResponse httpResponse = httpClient.execute(httpRequestBase);
        // 响应处理
        HttpEntity httpResponseEntity = httpResponse.getEntity();
        String responseEntityStr = EntityUtils.toString(httpResponseEntity);
        ((CloseableHttpResponse) httpResponse).close();
        System.out.println(responseEntityStr);
        System.out.println("___________________________________");
        return responseEntityStr;
    }

    private static HttpRequestBase getHttpRequestBase(String method, byte[] params) {
        HttpRequestBase httpRequestBase = null;
        if (POST.equals(method) && params != null) {
            httpRequestBase = new HttpPost();
            HttpEntity entity = new ByteArrayEntity(params,ContentType.MULTIPART_FORM_DATA);
            ((HttpPost) httpRequestBase).setEntity(entity);
        } else {
            httpRequestBase = new HttpRequestBase() {
                @Override
                public String getMethod() {
                    return method;
                }
            };
        }
        return httpRequestBase;
    }

    private static CookieStore setCookies(Cookie[] cookies, String domain) {
        CookieStore cookieStore = null;
        if (cookies != null && cookies.length > 0) {
            cookieStore = new BasicCookieStore();
            for (Cookie cookie : cookies) {
                BasicClientCookie basicClientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
                basicClientCookie.setDomain(domain);
                basicClientCookie.setPath(StringUtils.isNotBlank(cookie.getPath()) ? cookie.getPath() : "/");
                cookieStore.addCookie(basicClientCookie);
            }
        }
        return cookieStore;
    }

    private static HttpRequestBase setHeader(Map<String, String> headerMap, HttpRequestBase httpRequestBase) {
        if (!CollectionUtils.isEmpty(headerMap)) {
            headerMap.forEach(httpRequestBase::setHeader);
        }
        return httpRequestBase;
    }

    public static String getUrl() {
        return "";
    }

    public static String requestUrl(String url, String method, String domain) throws IOException {
        return requestUrl(url, method, domain, null, null, null);
    }

    public static String requestUrl(String url, String method, String domain, Map<String, String> headerMap, byte[] params) throws IOException {
        return requestUrl(url, method, domain, null, headerMap, params);
    }
}
