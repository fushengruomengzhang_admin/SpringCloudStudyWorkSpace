package cn.zfs.springcloud.aop.log;

import cn.zfs.springcloud.aop.model.LoggerModel;
import cn.zfs.springcloud.util.model.BaseResponse;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 通过表达式的方式实现切面编程
 */
@Aspect
@Component
@Order(1)
public class AopAspectLog {

    private Logger logger = LoggerFactory.getLogger(AopAspectLog.class);
    @Autowired
    private LoggerModel loggerModel;

    /**
     * Controller层切入点
     */
    @Pointcut("execution(* cn.zfs.springcloud.*.server.controller.*.*(..))")
    public void controllerLogger() {
    }

    /**
     * controller环绕通知
     * controller层使用环绕通知,记录日志内容,修改请求参数(空串、null值的处理),记录返回值日历
     *
     * @param joinPoint ..
     * @return ..
     * @throws Throwable ..
     */
    @Around("controllerLogger()")
    public Object aroundController(ProceedingJoinPoint joinPoint) throws Throwable {
        if (loggerModel.isOpenLogger()) {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (null == attributes) {
                return BaseResponse.fail("系统出现异常");
            }
            HttpServletRequest request = attributes.getRequest();
            logger.debug("请求主机ip:{}", request.getRemoteAddr());
            logger.debug("请求完整路径:{}", request.getRequestURL());
            logger.debug("请求方式:{}", request.getMethod());
            logger.debug("请求类名:{}", joinPoint.getTarget().getClass().getName());
            logger.debug("请求方法名:{}", joinPoint.getSignature().getName());
            logger.info("请求参数列表:{}", request.getParameterMap());
        }
        Object[] args = joinPoint.getArgs();
        Arrays.asList(args).forEach(arg -> arg = arg == null || StringUtils.isBlank(String.valueOf(arg)) ? null : arg);
        Object obj = joinPoint.proceed(args);
        logger.info("请求结果:{}", JSONObject.toJSONString(obj));
        return obj;
    }

    /**
     * service 层切入点
     */
    @Pointcut("execution(* cn.zfs.springcloud.*.server.service.*.*(..))")
    public void serviceLog() {
    }

    /**
     * service层环绕通知
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("serviceLog()")
    public Object aroundService(ProceedingJoinPoint joinPoint) throws Throwable {
        if (loggerModel.isOpenLogger()) {
            logger.debug("请求类名:{}", joinPoint.getTarget().getClass().getName());
            logger.debug("请求方法名:{}", joinPoint.getSignature().getName());
            logger.info("请求参数列表:{}", joinPoint.getArgs());
        }
        Object object = joinPoint.proceed();
        logger.info("返回值:{}", JSONObject.toJSONString(object));
        return object;
    }
}
