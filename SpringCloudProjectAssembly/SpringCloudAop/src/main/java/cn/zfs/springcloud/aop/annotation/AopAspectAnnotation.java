package cn.zfs.springcloud.aop.annotation;

import cn.zfs.springcloud.exception.global.GlobalException;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 通过注解的方式实现切面编程
 */
@Aspect
@Component
public class AopAspectAnnotation {

    private Logger logger = LoggerFactory.getLogger(AopAspectAnnotation.class);

    @Pointcut("@annotation(cn.zfs.springcloud.aop.annotation.Annotation)")
    public void annotationPointCut(){};

    @Around("annotationPointCut()")
    public Object before(ProceedingJoinPoint joinPoint) throws GlobalException {
        logger.info("通过注解的方式进行前置拦截,可以拦截登录、校验等等");
        try {
            if (joinPoint.getArgs().length > 1) {
                return joinPoint.proceed();
            } else {
                return BaseResponse.fail("XXX不通过");
            }
        } catch (Throwable throwable) {
            throw new GlobalException("XXXX出现异常", "异常了啊奥奥啊啊啊");
        }
    }

}
