package cn.zfs.springcloud.aop.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoggerModel {

    @Value("${me.open.log}")
    private boolean openLogger;

    public boolean isOpenLogger() {
        return openLogger;
    }
}
