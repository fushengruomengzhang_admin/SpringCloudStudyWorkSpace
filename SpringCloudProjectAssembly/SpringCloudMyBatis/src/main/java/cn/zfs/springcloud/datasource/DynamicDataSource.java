package cn.zfs.springcloud.datasource;

import cn.zfs.springcloud.datasource.contextholder.DataSourceContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    private Logger logger = LoggerFactory.getLogger(DynamicDataSource.class);

    @Override
    protected Object determineCurrentLookupKey() {
        String lookupKey = DataSourceContextHolder.getDetermineCurrentLookupKey();
        logger.debug("当前数据源为:{}", lookupKey);
        return DataSourceContextHolder.getDetermineCurrentLookupKey();
    }

}