package cn.zfs.springcloud.datasource.contextholder;

import cn.zfs.springcloud.datasource.init.InitData;

/**
 * 统计事务个数
 */
public class TransactionNumContextHolder {

    private final static ThreadLocal<Integer> TRANSACTION_NUM_THREAD_LOCAL = new ThreadLocal<>();

    public static void setTransactionNum(int num, boolean thisMethodOpenTrancation) {
        if (TRANSACTION_NUM_THREAD_LOCAL.get() == null) { // 初始化开启事务的个数(即启动方法内部调用了几个需要开启事务的方法)
            TRANSACTION_NUM_THREAD_LOCAL.set(num);
        } else if (num > InitData.ONE) { // 递增开启事务的个数(即启动方法内部调用的开启事务的方法中也需要开启多个事务,)
            if (thisMethodOpenTrancation) {
                // 启动方法需要开启事务,并且方法内部调用的方法也有需要开启事务的方法
                TRANSACTION_NUM_THREAD_LOCAL.set(TRANSACTION_NUM_THREAD_LOCAL.get() + num - InitData.ONE);
            } else {
                // 启动方法不需要开启事务,但是方法内部有需要开启事务的方法
                TRANSACTION_NUM_THREAD_LOCAL.set(TRANSACTION_NUM_THREAD_LOCAL.get() + num);
            }
        } else if (num == InitData.NEGATIVEONE) { // 有一个方法可以提交事务,总数-1
            TRANSACTION_NUM_THREAD_LOCAL.set(TRANSACTION_NUM_THREAD_LOCAL.get() + num);
        }
    }

    public static Integer getTransactionNum() {
        return TRANSACTION_NUM_THREAD_LOCAL.get();
    }

    public static void removeTransactionNum() {
        TRANSACTION_NUM_THREAD_LOCAL.remove();
    }
}
