package cn.zfs.springcloud.datasource.init;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 初始化数据
 */
@Component
public class InitData {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int INITIALCAPACITY = ONE;
    public static final int NEGATIVEONE = -1;

    @Value("${db.default.name}")
    private String defaultDataSourceName;

    public String getDefaultDataSourceName() {
        return defaultDataSourceName;
    }

}
