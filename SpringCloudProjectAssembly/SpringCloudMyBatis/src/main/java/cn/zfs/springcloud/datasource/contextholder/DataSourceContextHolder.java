package cn.zfs.springcloud.datasource.contextholder;

import cn.zfs.springcloud.datasource.init.InitData;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据源
 */
@SuppressWarnings("WeakerAccess")
public class DataSourceContextHolder {

    private final static ThreadLocal<String> contextHolder = new ThreadLocal<>();

    // 设置数据源名
    public static void setDetermineCurrentLookupKey(String dbName) {
        contextHolder.set(dbName);
    }

    // 获取数据源名
    public static String getDetermineCurrentLookupKey() {
        return contextHolder.get();
    }

    // 清除数据源名
    public static void clearDetermineCurrentLookupKey() {
        contextHolder.remove();
    }
}
