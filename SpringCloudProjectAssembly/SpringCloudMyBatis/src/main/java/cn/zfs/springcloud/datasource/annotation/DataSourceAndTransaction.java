package cn.zfs.springcloud.datasource.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataSourceAndTransaction {

    /**
     * 全局: 是否需要开启事务
     * 默认: 需要开启事务
     * 解释: 如果需要开启事务,则为启动方法开启事务,并为涉及到该注解的内部方法开启事务,
     * 注意: 该属性的优先级低于 thisMethodOpenTransaction属性
     * @return Boolean
     */
    boolean openTransaction() default true;

    /**
     * 局部: 当前方法是否需要开启事务
     * 默认: 需要开启事务
     * 解释: 默认当前方法中也执行了修改操作
     * 注意: 该属性的优先级高于 openTransaction属性
     * @return Boolean
     */
    boolean thisMethodOpenTransaction() default true;

    /**
     * 当前方法开启事务的个数
     * 即: 当前方法内部涉及到几个需要开启事务的方法
     * @return int
     */
    int openTransactionNum() default 1;

    // 数据源名称
    String dataSourceName() default "";

    /**
     * 是否是分布式项目
     * @return Boolean
     */
    boolean isDistributed() default false;

}
