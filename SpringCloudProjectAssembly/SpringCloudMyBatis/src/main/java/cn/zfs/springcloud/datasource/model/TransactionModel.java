package cn.zfs.springcloud.datasource.model;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;

/**
 * 事务对象
 */
public class TransactionModel {

    public TransactionModel(String dataSourceName, DataSourceTransactionManager dataSourceTransactionManager, TransactionStatus transactionStatus) {
        this.dataSourceName = dataSourceName;
        this.dataSourceTransactionManager = dataSourceTransactionManager;
        this.transactionStatus = transactionStatus;
    }

    // 数据源
    private String dataSourceName;
    // 事务控制管理器
    private DataSourceTransactionManager dataSourceTransactionManager;
    // 事务状态
    private TransactionStatus transactionStatus;

    public String getDataSourceName() {
        return dataSourceName;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    public DataSourceTransactionManager getDataSourceTransactionManager() {
        return dataSourceTransactionManager;
    }

    public void setDataSourceTransactionManager(DataSourceTransactionManager dataSourceTransactionManager) {
        this.dataSourceTransactionManager = dataSourceTransactionManager;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
