package cn.zfs.swagger.ui.entity;

import cn.zhangfusheng.api.annotation.ParameterAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: TestRequestEntity
 * @Author: ZFS
 * @Date: 2018/9/28 12:10
 */
@ApiModel(value = "TestRequestEntity", discriminator = "请求求参数传递对象")
@Data
public class TestRequestEntity {

    @ParameterAnnotation(name = "id",desc = "id")
    @ApiModelProperty(name = "id", value = "id", required = true)
    private int id;

    @ParameterAnnotation(name = "testName",desc = "testName",required = true)
    @ApiModelProperty(name = "testName", value = "名称")
    private String testName;
}
