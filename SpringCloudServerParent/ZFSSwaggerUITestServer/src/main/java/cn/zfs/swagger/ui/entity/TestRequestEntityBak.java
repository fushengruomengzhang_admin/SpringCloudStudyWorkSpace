package cn.zfs.swagger.ui.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: TestRequestEntity
 * @Author: ZFS
 * @Date: 2018/9/28 12:10
 */
@ApiModel(value = "TestRequestEntity",discriminator = "请求求参数传递对象")
@Data
public class TestRequestEntityBak {

    @ApiModelProperty(name = "id",value = "id")
    private int id;

    @ApiModelProperty(name = "testName",value = "名称")
    private String testName;

    private TestRequestEntityBak testRequestEntityBak;
}
