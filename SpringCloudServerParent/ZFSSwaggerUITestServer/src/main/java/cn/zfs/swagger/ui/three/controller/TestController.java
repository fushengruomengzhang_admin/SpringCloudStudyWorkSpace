package cn.zfs.swagger.ui.three.controller;

import cn.zfs.swagger.ui.entity.TestRequestEntity;
import cn.zhangfusheng.api.annotation.ClassAnnotation;
import cn.zhangfusheng.api.annotation.MethodAnnotation;
import cn.zhangfusheng.api.annotation.ParameterAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TestController
 * @Author: ZFS
 * @Date: 2018/10/19 17:15
 */
@Slf4j
@ClassAnnotation(desc = "一号测试用例", path = "t1")
@RestController
@RequestMapping("t1")
public class TestController {

    @MethodAnnotation(path = "test1")
    @GetMapping("test1")
    public Object test1() {
        return "Success";
    }

    @MethodAnnotation(path = "test2", parameterAnnotations = {
            @ParameterAnnotation(name = "userName"),
            @ParameterAnnotation(name = "password")
    })
    @GetMapping("test2")
    public Object test2(String userName, String password) {
        log.info("username={},password={}", userName, password);
        return "Success";
    }

    @MethodAnnotation(path = "test3", parameterAnnotations = {
            @ParameterAnnotation(name = "testRequestEntity", type = TestRequestEntity.class)
    })
    @GetMapping("test3")
    public Object test3(TestRequestEntity testRequestEntity) {
        return testRequestEntity;
    }

    @MethodAnnotation(path = "test4", method = "post", parameterAnnotations = {
            @ParameterAnnotation(name = "testRequestEntity", type = TestRequestEntity.class)
    })
    @PostMapping("test4")
    public Object test4(TestRequestEntity testRequestEntity) {
        return testRequestEntity;
    }
}
