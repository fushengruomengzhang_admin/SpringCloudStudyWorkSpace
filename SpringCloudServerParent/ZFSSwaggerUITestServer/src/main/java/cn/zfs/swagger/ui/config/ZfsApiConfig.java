package cn.zfs.swagger.ui.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @ClassName: ZfsApiConfig
 * @Author: ZFS
 * @Date: 2018/10/19 17:50
 */
@Slf4j
@Configuration
@ComponentScan("cn.zhangfusheng.api.rest")
public class ZfsApiConfig implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        log.info("初始化zfs-zpi");
        cn.zhangfusheng.api.config.ZfsApiConfig.initZfsApiConfig("cn.zfs.swagger.ui");
        HashMap<String, String> groupPackageMap = new HashMap<>();
        groupPackageMap.put("one","cn.zfs.swagger.ui.one.controller");
        groupPackageMap.put("two","cn.zfs.swagger.ui.two.controller");
        groupPackageMap.put("three","cn.zfs.swagger.ui.three.controller");
        cn.zhangfusheng.api.config.ZfsApiConfig.initZfsApiConfig(groupPackageMap);

    }
}
