package cn.zfs.swagger.ui.two.controller;

import cn.zfs.springcloud.util.model.BaseResponse;
import cn.zfs.swagger.ui.entity.TestRequestEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

/**
 * @ClassName: TestController
 * @Author: ZFS
 * @Date: 2018/9/27 14:18
 */
@RestController
@Api(tags = "第二组测试用例1号")
@RequestMapping("group2/test1")
public class TestControllerGroup2_1 {

    @ApiOperation(value = "get 无参数请求方式11111111111111111111", notes = "get 无参数请求方式", produces = "application/json")
    @GetMapping("test1")
    public String test1(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            // System.out.println(cookie.getValue() + "==" + cookie.getName());
        }
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            System.out.println(headerName + "==" + request.getHeader(headerName));
        }
        return "Success";
    }

    @ApiOperation(value = "N中请求方式", notes = "N中请求方式", produces = "application/json")
    @RequestMapping("test2")
    public String test2() {
        return "Success";
    }

    @ApiOperation(value = "post 传递对象请求方式", notes = "post 传递对象请求方式", produces = "application/json")
    @PostMapping("test3")
    public BaseResponse test3(TestRequestEntity testRequestEntity) {
        return BaseResponse.success(testRequestEntity);
    }


}
