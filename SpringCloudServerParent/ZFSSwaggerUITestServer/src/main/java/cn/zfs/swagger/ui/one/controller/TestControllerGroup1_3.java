package cn.zfs.swagger.ui.one.controller;

import cn.zfs.springcloud.util.model.BaseResponse;
import cn.zfs.swagger.ui.entity.TestRequestEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: TestController
 * @Author: ZFS
 * @Date: 2018/9/27 14:18
 */
@RestController
@Api(tags = {"第一组测试用例2号_1", "第一组测试用例2号_2"})
@RequestMapping("test3")
public class TestControllerGroup1_3 {

    @ApiOperation(value = "get 无参数请求方式", notes = "get 无参数请求方式", produces = "application/json")
    @GetMapping("test1")
    public String test1() {
        return "Success";
    }

    @ApiOperation(value = "N中请求方式", notes = "N中请求方式", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 405,
                    message = "405 错误",
                    response = BaseResponse.class,
                    responseHeaders = {
                            @ResponseHeader(name = "test", description = "tes1111", response = BaseResponse.class, responseContainer = "Set")
                    }
            ), // 在默认Response的基础上增加新的Response说明
            @ApiResponse(code = 404, message = "404 错误") // 覆盖404错误的说明
    })
    @ApiParam
    @RequestMapping("test2")
    public Object test2(@RequestParam(required = true) String name) {
        return new TestRequestEntity();
    }

    @ApiOperation(value = "post 传递对象请求方式", notes = "post 传递对象请求方式", produces = "application/json")
    @PostMapping("test3")
    public BaseResponse test3(TestRequestEntity testRequestEntity) {
        return BaseResponse.success(testRequestEntity);
    }


}
