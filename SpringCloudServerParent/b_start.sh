#!/bin/bash
# rm -rf
echo "是否需要删除SpringCloudStudyWorkSpace?y/n"
read yn
if [ $yn = 'y' ];then
    rm -rf SpringCloudStudyWorkSpace
fi

# clone
echo "是否需要重新克隆仓库?y/n"
read yn
if [ $yn = 'y' ];then
    git clone git@gitee.com:fushengruomengzhang_admin/SpringCloudStudyWorkSpace.git
fi
# cd SpringCloudStudyWorkSpace and mvn clean install
cd SpringCloudStudyWorkSpace
echo "是否需要打包?y/n"
read yn
if [ $yn = 'y' ];then
    mvn clean install -Dmaven.test.skip=true
fi
cd ../
# cp *.jar to jar
path=`pwd`/SpringCloudServerParent/
echo "是否需要复制jar包?y/n"
read yn
if [ $yn = 'y' ];then
    declare -a list_result=(`find "$path" -name *.jar`)
    for result in ${list_result[@]}
    do
        for key in ${!result[@]}
        do
            echo cp -rf ${result[$key]} jar/
            rm -rf jar/${result[$key]}
            cp -rf ${result[$key]} jar/
        done
    done
fi
# start and shutdown
declare -a list_result_start=(
"SpringCloudUserServer-1.0-SNAPSHOT.jar"
"SpringCloudLoginServer-1.0-SNAPSHOT.jar"
"SpringCloudFileUploadServer-1.0-SNAPSHOT.jar"
"SpringCloudTaskServer-1.0-SNAPSHOT.jar"
"SpringCloudAuthServer-1.0-SNAPSHOT.jar"
)
declare -a list_result_server_name=(
"user-server"
"loggin-server"
"file-server"
"task-server"
"auth-server"
)
declare -a list_result_shutdown=(
"172.21.0.16:9095/shutdown"
"172.21.0.16:9096/shutdown"
"172.21.0.16:9097/shutdown"
"172.21.0.16:9098/shutdown"
)
declare -a list_result_info=(
"172.21.0.16:9095/info"
"172.21.0.16:9096/info"
"172.21.0.16:9097/info"
"172.21.0.16:9098/info"
)
cd jar
echo `pwd`
for i in ${!list_result_start[@]}
do
    echo "********************************************************************************************************"
    echo curl -u name:password -X POST -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_shutdown[$i]}
    echo `curl -u name:password -X POST -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_shutdown[$i]}`
    java -jar ${list_result_start[$i]} > "../log/${list_result_server_name[$i]}"-log.log 2>&1 &
    sleep 3s
done
