package cn.zfs.fast.dao;

import cn.zfs.fast.entity.FilePath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ClassName: FilePathDao
 * @Author: ZFS
 * @Date: 2018/11/1 16:12
 */
@Repository
public interface FilePathDao extends JpaRepository<FilePath, Long> {

    List<FilePath> findByTypeOrFileName(String type, String fileName);

    @Query("select distinct type from FilePath ")
    List<String> findTypes();
}

