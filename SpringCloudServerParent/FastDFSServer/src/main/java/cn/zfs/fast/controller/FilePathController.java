package cn.zfs.fast.controller;

import cn.zfs.fast.entity.FilePath;
import cn.zfs.fast.service.FilePathService;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: FilePathController
 * @Author: ZFS
 * @Date: 2018/11/1 16:17
 */
@RestController
@RequestMapping("show")
public class FilePathController {

    @Autowired
    FilePathService filePathService;

    @GetMapping("findAll")
    public List<FilePath> findAll(FilePath filePath){
        return filePathService.findByTypeOrFileName(filePath);
    }

    @GetMapping("findTypes")
    public List<String> findTypes(){
        return filePathService.findType();
    }
}
