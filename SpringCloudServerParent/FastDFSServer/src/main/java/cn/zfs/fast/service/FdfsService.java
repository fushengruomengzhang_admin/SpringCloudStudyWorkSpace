package cn.zfs.fast.service;

import cn.zfs.fast.entity.FilePath;
import cn.zfs.fast.util.FdfsUtils;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * @ClassName: FdfsController
 * @Author: ZFS
 * @Date: 2018/11/1 16:35
 */
@RestController
public class FdfsService {

    @Resource
    FdfsUtils fdfsUtils;
    @Autowired
    FilePathService filePathService;

    public BaseResponse fileUpdaload(MultipartFile file) throws IOException {
        String uploadFilePath = fdfsUtils.uploadFile(file);
        FilePath filePath = new FilePath();
        filePath.setFileName(file.getOriginalFilename());
        filePath.setUrl(uploadFilePath);
        filePath.setType(file.getContentType());
        filePathService.save(filePath);
        return BaseResponse.success(uploadFilePath);
    }
}
