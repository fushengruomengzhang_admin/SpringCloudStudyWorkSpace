package cn.zfs.fast.controller;

import cn.zfs.fast.service.FdfsService;
import cn.zfs.springcloud.util.model.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @ClassName: FdfsController
 * @Author: ZFS
 * @Date: 2018/11/1 16:35
 */
@Slf4j
@RestController
@RequestMapping("fdfs")
public class FdfsController {

    @Resource
    FdfsService fdfsService;

    @PostMapping("fileUpdaload")
    public BaseResponse fileUpdaload(MultipartFile file) throws IOException {
        return fdfsService.fileUpdaload(file);
    }
}
