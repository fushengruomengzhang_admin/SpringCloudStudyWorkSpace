package cn.zfs.fast.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @ClassName: FilePath
 * @Author: ZFS
 * @Date: 2018/11/1 15:48
 */
@Data
@Entity
public class FilePath extends BaseEntity {

    private String fileName;
    private String type;
    @Column(length = 500)
    private String url;
}