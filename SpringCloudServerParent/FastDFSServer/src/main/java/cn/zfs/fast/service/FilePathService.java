package cn.zfs.fast.service;

import cn.zfs.fast.dao.FilePathDao;
import cn.zfs.fast.entity.FilePath;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: FilePathService
 * @Author: ZFS
 * @Date: 2018/11/1 16:14
 */
@Service
public class FilePathService {

    @Autowired
    FilePathDao filePathDao;

    public List<FilePath> findByTypeOrFileName(FilePath filePath) {
        return filePathDao.findByTypeOrFileName(filePath.getType(), filePath.getFileName());
    }

    public List<String> findType(){
        return filePathDao.findTypes();
    }

    public BaseResponse save(FilePath filePath) {
        filePath = filePathDao.save(filePath);
        if (filePath.getId() > 0) {
            return BaseResponse.success("添加成功", "");
        }
        return BaseResponse.fail("添加失败");
    }
}
