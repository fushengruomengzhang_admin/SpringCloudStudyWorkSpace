package cn.zfs.springcloud.user.server.dao;

import cn.zfs.springcloud.datasource.annotation.SwitchDataSource;
import cn.zfs.springcloud.userserver.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * 多数据源和事务的管理
 */
@Mapper
@Repository
public interface DataSourceAndTransactionDao {

    @SwitchDataSource(dataSourceName = "db_dev")
    @Insert("INSERT INTO `user` (`user_name`, `password`, `update_time`, `create_time`) " +
            "VALUES (#{userName}, #{password}, #{createTime}, #{updateTime})")
    public int insert(User user);

    @Update("UPDATE `user` SET `user_name`=#{userName}, `password`=#{password}, " +
            "`update_time`=#{updateTime}, `create_time`=#{createTime} WHERE (`id`=#{id})")
    public int update(User user);
}
