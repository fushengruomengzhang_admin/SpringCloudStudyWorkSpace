package cn.zfs.springcloud.user.server.service.impl;

import cn.zfs.springcloud.datasource.annotation.ManagementTransaction;
import cn.zfs.springcloud.datasource.annotation.SwitchDataSource;
import cn.zfs.springcloud.exception.global.GlobalException;
import cn.zfs.springcloud.user.server.dao.UserDao;
import cn.zfs.springcloud.user.server.service.UserService;
import cn.zfs.springcloud.userserver.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
@Service
public class UserServiceImpl implements UserService {

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Value("${spring.rabbitmq.addresses}")
    private String properties;

    @Override
    public String getProperties(){
        return properties;
    }

    @Override
    public User findByname(String name) {
        return userDao.findByName(name);
    }

    @Override
    @ManagementTransaction
    @SwitchDataSource(dataSourceName = "db_dev")
    public int insert(){
        return userDao.insert();
    }

    @Override
    public List<User> findByNames(String name){
        return userDao.findByNames(name);
    }

    @Override
    public List<User> findNameByNames(String name){
        return userDao.findNameByNames(name);
    }

    @Override
    public Map<String, Object> getUserList() {
        logger.info("\"用户服务--获取用户列表");
        Map<String, Object> resultMap = new HashMap<>();
        List<User> users = new ArrayList<>();
        users.add(new User(1, "user", "password"));
        resultMap.put("success", true);
        resultMap.put("msg", "用户服务--获取用户列表");
        resultMap.put("data", users);
        return resultMap;
    }

    @Override
    public Map<String, Object> getUserByName(String userName){
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("success", true);
        resultMap.put("msg", "用户服务--获取用户列表");
        resultMap.put("data", new User(1,"中文测试","编码测试"));
        return resultMap;
    }

    @Override
    public Map<String, Object> throwByZero() throws Exception {
        throw new GlobalException("故意制造异常:{},{}","{}{}测试");
    }
}
