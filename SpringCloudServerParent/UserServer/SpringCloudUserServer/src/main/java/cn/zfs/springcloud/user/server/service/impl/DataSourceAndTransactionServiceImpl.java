package cn.zfs.springcloud.user.server.service.impl;

import cn.zfs.springcloud.datasource.annotation.DataSourceAndTransaction;
import cn.zfs.springcloud.user.server.dao.DataSourceAndTransactionDao;
import cn.zfs.springcloud.user.server.service.DataSourceAndTransactionService;
import cn.zfs.springcloud.userserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据源切换和事务管理测试
 */
@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
@Service
public class DataSourceAndTransactionServiceImpl implements DataSourceAndTransactionService {

    @Autowired
    private DataSourceAndTransactionDao dataSourceAndTransactionDao;
    @Autowired
    private DataSourceAndTransactionService dataSourceAndTransactionService;

    @Override
    @DataSourceAndTransaction(dataSourceName = "db_master")
    public int switchDataSource_db_master(User user) {
        System.out.println("数据源切换至: db_master 库执行insert操作");
        int id = dataSourceAndTransactionDao.insert(user);
//        int a = 1 / 0;
        return id;
    }

    @Override
    @DataSourceAndTransaction(dataSourceName = "db_dev")
    public int switchDataSource_db_dev(User user) {
        System.out.println("数据源切换至: db_dev 库执行 insert 操作");
        int id = dataSourceAndTransactionDao.insert(user);
        return id;
    }

    @Override
    public int switchDataSource_3_db(User user) {
        String userName = user.getUserName();
        user.setUserName(userName + "/db_master");
        int i = dataSourceAndTransactionService.switchDataSource_db_master(user); // db_master 1
        user.setUserName(userName + "/db_dev");
        int i1 = dataSourceAndTransactionService.switchDataSource_db_dev(user);// db_dev 1
        user.setUserName(userName + "/db_dev-zhu");
        int insert = dataSourceAndTransactionDao.insert(user); // db_master 1
        return i + i1 + insert;
    }

    @Override
    public int switchDataSource_4_db(User user) {
        String userName = user.getUserName();
        user.setUserName(userName + "_4_1");
        int insert = dataSourceAndTransactionDao.insert(user); // db_dev 1
        user.setUserName(userName + "_4_2");
        int i = dataSourceAndTransactionService.switchDataSource_db_master(user); // db_master 1
        int a = 1 / 0;
        user.setUserName(userName + "_4_3");
        int i1 = dataSourceAndTransactionService.switchDataSource_db_dev(user); // db_dev 1
        return i + i1 + insert;
    }

    @Override
    public int switchDataSource_5_db(User user) {
        int i = dataSourceAndTransactionService.switchDataSource_3_db(user); // db_master 2 db_dev 1
        int insert = dataSourceAndTransactionDao.insert(user); // db_dev 1
        return i + insert;
    }

    @Override
    public int switchDataSource_6_db(User user) {
        int insert = dataSourceAndTransactionDao.insert(user);
        int i = dataSourceAndTransactionService.switchDataSource_3_db(user);
        return i + insert;
    }

}
