package cn.zfs.springcloud.user.server.service;

import cn.zfs.springcloud.datasource.annotation.SwitchDataSource;
import cn.zfs.springcloud.userserver.model.User;

/**
 * 数据源切换和事务管理测试
 */
@SuppressWarnings({"JavaDoc", "UnusedReturnValue"})
public interface DataSourceAndTransactionService {

    /**
     * 测试数据源切换 1-1 单个数据源
     * 结论: 可以指定数据源
     * @param user
     * @return
     */
    int switchDataSource_db_master(User user);

    /**
     * 测试数据源切换 1-2 单个数据源
     * 结论: 可以指定数据源
     * @param user
     * @return
     */
    int switchDataSource_db_dev(User user);

    /**
     * 测试数据源切换 1-3 多个数据源,方法中调用两个方法,之后,在使用自身的方法指定的数据源修改表数据
     * 结论: 可以按照指定的数据源修改表数据
     * @param user
     * @return
     */
    int switchDataSource_3_db(User user);

    /**
     * 测试数据源切换 1-4 多个数据源,先自身操作表数据,然后调用 其他两个方法操作表数据
     * 结论: 可以按照指定的数据源修改表数据
     * @param user
     * @return
     */
    int switchDataSource_4_db(User user);

    /**
     * 测试数据源切换 1-5 多个数据源,方法中调用的方法中又调用方法,然后自身在使用指定的数据源修改表数据
     * 结论: 可以按照指定的数据源修改表数据
     * @param user
     * @return
     */
    int switchDataSource_5_db(User user);

    /**
     * 测试数据源切换 1-6 多个数据源,自身使用指定的数据源修改表数据,然后在调用方法中在调用方法
     * 结论: 可以按照指定的数据源修改表数据
     * @param user
     * @return
     */
    int switchDataSource_6_db(User user);
}
