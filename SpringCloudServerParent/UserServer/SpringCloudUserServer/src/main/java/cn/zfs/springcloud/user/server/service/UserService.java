package cn.zfs.springcloud.user.server.service;


import cn.zfs.springcloud.userserver.model.User;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface UserService {
    String getProperties();

    User findByname(String name);

    int insert();

    List<User> findByNames(String name);

    List<User> findNameByNames(String name);

    Map<String, Object> getUserList();

    Map<String, Object> getUserByName(String userName);

    Map<String, Object> throwByZero() throws Exception;
}
