package cn.zfs.springcloud.user.server.dao;

import cn.zfs.springcloud.userserver.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserDao {

    /**
     * 通过配置文件的方式查询
     * @param name
     * @return
     */
    public User findByName(@Param("name") String name);

    /**
     * 通过注解的方式查询 全部字段
     * @param name
     * @return
     */
    @Select("select * from user where userName=#{name}")
    public List<User> findByNames(@Param("name") String name);

    @Select("select userName from user where userName=#{name}")
    public List<User> findNameByNames(@Param("name") String name);

    @Insert("INSERT INTO `user-server-bak`.`user` (`userName`, `password`) VALUES ('lisi1', 'zfs')")
    public int insert();
}
