package cn.zfs.springcloud.user.server.controller;


import cn.zfs.springcloud.user.server.feign.UserServerFeignClient;
import cn.zfs.springcloud.user.server.service.UserService;
import cn.zfs.springcloud.userserver.model.User;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@SuppressWarnings("unchecked")
@RestController
public class UserController implements UserServerFeignClient {

    @Autowired
    private UserService userService;

    @GetMapping("getProperties")
    public String getProperties(){
        return userService.getProperties();
    }

    @Override
    @GetMapping("findByName")
    public BaseResponse<User> findByName(@RequestParam String name) {
        return BaseResponse.success(userService.findByname(name));
    }

    @Override
    @GetMapping("findByNames")
    public BaseResponse<List<User>> findByNames(@RequestParam String name) {
        return BaseResponse.success(userService.findByNames(name));
    }

    @Override
    @GetMapping("findNameByNames")
    public BaseResponse<List<User>> findNameByNames(@RequestParam String name) {
        return BaseResponse.success(userService.findNameByNames(name));
    }

    @Override
    @GetMapping("getUserList")
    public Map<String, Object> getUserList() {
        return userService.getUserList();
    }

    @Override
    @GetMapping("getUserByName")
    public Map<String, Object> getUserByName(@RequestParam String userName){
        return userService.getUserByName(userName);
    }

    @Override
    @RequestMapping("throwByZero")
    public Map<String, Object> throwByZero() throws Exception {
        return userService.throwByZero();
    }

    @RequestMapping("insert")
    public int insert(){
        return userService.insert();
    }
}
