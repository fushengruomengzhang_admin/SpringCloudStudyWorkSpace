package cn.zfs.springcloud;

import cn.zfs.springcloud.user.server.service.DataSourceAndTransactionService;
import cn.zfs.springcloud.userserver.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SuppressWarnings("WeakerAccess")
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplication {

    @Autowired
    private DataSourceAndTransactionService dataSourceAndTransactionService;

    User user = null;
    long openTime = 0L;
    long endTime = 0L;

    @Before
    public void before() {
        user = new User();
        user.setUpdateTime(new Date());
        user.setCreateTime(new Date());
        openTime = new Date().getTime();
    }

    @After
    public void after() {
        endTime = new Date().getTime();
        System.out.println(endTime - openTime);
    }

    @Test
    public void test_switchDataSource() {
        user.setUserName("db_master");
        user.setPassword("master_db");
        dataSourceAndTransactionService.switchDataSource_db_master(user);
    }

    @Test
    public void test_switchDataSource_3() {
        user.setUserName("test");
        user.setPassword("test");
        int i = dataSourceAndTransactionService.switchDataSource_3_db(user);
        System.out.println(i);
    }

    @Test
    public void test_switchDataSource_4(){
        user.setUserName("test_4");
        user.setPassword("test_4");
        int i = dataSourceAndTransactionService.switchDataSource_4_db(user);
        System.out.println(i);
    }

    @Test
    public void test_switchDataSource_5(){
        user.setUserName("test_5");
        user.setPassword("test_5");
        int i = dataSourceAndTransactionService.switchDataSource_5_db(user);
        System.out.println(i);
    }

    @Test
    public void test_switchDataSource_6(){
        user.setUserName("test_6");
        user.setPassword("test_6");
        int i = dataSourceAndTransactionService.switchDataSource_6_db(user);
        System.out.println(i);
    }
}
