package cn.zfs.springcloud.userserver.model;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class User {

    private Integer id;
    private String userName;
    private String password;
    private Date updateTime;
    private Date createTime;

    public User() {
    }

    public User(Integer id, String userName, String password) {
        this.id = id;
        this.userName = userName;
        this.password = password;
    }
}
