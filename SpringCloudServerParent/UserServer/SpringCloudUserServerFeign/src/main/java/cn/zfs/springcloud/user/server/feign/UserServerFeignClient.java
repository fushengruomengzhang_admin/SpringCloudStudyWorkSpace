package cn.zfs.springcloud.user.server.feign;


import cn.zfs.springcloud.user.server.config.UserFeignLoggerConfig;
import cn.zfs.springcloud.user.server.hystrix.UserServerHystrix;
import cn.zfs.springcloud.userserver.model.User;
import cn.zfs.springcloud.util.model.BaseResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@FeignClient(
        name = "user-server",
        fallbackFactory = UserServerHystrix.class,
        configuration = {UserFeignLoggerConfig.class})
public interface UserServerFeignClient {

    @GetMapping("findByName")
    BaseResponse<User> findByName(@RequestParam String name);

    @GetMapping("findByNames")
    BaseResponse<List<User>> findByNames(@RequestParam String name);

    @GetMapping("findNameByNames")
    BaseResponse<List<User>> findNameByNames(@RequestParam String name);

    @GetMapping("getUserList")
    Map<String, Object> getUserList();

    @GetMapping("getUserByName")
    Map<String, Object> getUserByName(@RequestParam("userName") String userName);

    @GetMapping("throwByZero")
    Map<String, Object> throwByZero() throws Exception;
}
