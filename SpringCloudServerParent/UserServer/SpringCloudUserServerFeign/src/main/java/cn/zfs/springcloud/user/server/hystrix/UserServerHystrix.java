package cn.zfs.springcloud.user.server.hystrix;

import cn.zfs.springcloud.user.server.feign.UserServerFeignClient;
import cn.zfs.springcloud.userserver.model.User;
import cn.zfs.springcloud.util.model.BaseResponse;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class UserServerHystrix implements FallbackFactory<UserServerFeignClient> {

    private Logger logger = LoggerFactory.getLogger(UserServerHystrix.class);

    @Override
    public UserServerFeignClient create(Throwable cause) {
        logger.info("做个记号测试一下");
        return new UserServerFeignClient() {
            @Override
            public BaseResponse<User> findByName(String name) {
                return null;
            }

            @Override
            public BaseResponse<List<User>> findByNames(String name) {
                return null;
            }

            @Override
            public BaseResponse<List<User>> findNameByNames(String name) {
                return null;
            }

            @Override
            public Map<String, Object> getUserList() {
                logger.error("获取用户列表--走--服务降级", cause);
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("msg", "服务降级");
                return resultMap;
            }

            @Override
            public Map<String, Object> getUserByName(String userName) {
                logger.error("获取用户姓名--走--服务降级", cause);
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("msg", "服务降级");
                return resultMap;
            }

            @Override
            public Map<String, Object> throwByZero() throws Exception {
                logger.error("故意制造异常--走--服务降级", cause);
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("msg", "服务降级");
                return resultMap;
            }
        };
    }
}
