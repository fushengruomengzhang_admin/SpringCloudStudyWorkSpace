package cn.zfs.springcloud.auth.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Administrator
 */
@SuppressWarnings({"SpringFacetCodeInspection", "JavaDoc"})
@Configuration
@ComponentScan("cn.zfs.springcloud.auth.server.config")
public class AuthFeignLoggerConfig {

    /**
     * feign 的日志记录
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        /* NONE, 不记录日志 (默认)。
        BASIC, 只记录请求方法和URL以及响应状态代码和执行时间。
        HEADERS, 记录请求和应答的头的基本信息。
        FULL, 记录请求和响应的头信息，正文和元数据。*/
        return Logger.Level.FULL;
    }
}