package cn.zfs.springcloud.auth.feign;

import cn.zfs.springcloud.auth.config.AuthFeignLoggerConfig;
import cn.zfs.springcloud.auth.hystrix.AuthServerHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(
        name = "auth-server",
        fallbackFactory = AuthServerHystrix.class,
        configuration = {AuthFeignLoggerConfig.class})
public interface AuthServerFeignClient {
}
