package cn.zfs.springcloud.auth.hystrix;

import cn.zfs.springcloud.auth.feign.AuthServerFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AuthServerHystrix implements FallbackFactory<AuthServerFeignClient> {
    @Override
    public AuthServerFeignClient create(Throwable cause) {
        return new AuthServerFeignClient() {
        };
    }
}
