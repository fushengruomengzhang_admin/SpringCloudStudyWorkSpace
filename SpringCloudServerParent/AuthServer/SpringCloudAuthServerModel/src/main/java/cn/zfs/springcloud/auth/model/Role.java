package cn.zfs.springcloud.auth.model;

import cn.zfs.springcloud.auth.BaseEntity;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Data
@ToString(callSuper = true)
@Entity
public class Role extends BaseEntity {

    // 项目
    private String projectCode;
    // 角色名称
    private String roleName;
    // 角色编码
    @Column(unique = true)
    private String roleCode;
}
