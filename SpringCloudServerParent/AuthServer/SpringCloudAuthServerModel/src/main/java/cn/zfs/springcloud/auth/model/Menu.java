package cn.zfs.springcloud.auth.model;

import cn.zfs.springcloud.auth.BaseEntity;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import java.util.Date;

@Data
@ToString(callSuper = true)
@Entity
public class Menu extends BaseEntity {

    // 项目编码
    private String projectCode;
    // 菜单编码
    private String menuCode;
    // 菜单名称
    private String menuName;
    // 菜单类别 1页面 2按钮 3其他
    private Integer menuType;
}
