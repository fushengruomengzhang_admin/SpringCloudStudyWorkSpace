package cn.zfs.springcloud.auth.model;

import cn.zfs.springcloud.auth.BaseEntity;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * 项目
 */
@Data
@ToString(callSuper = true)
@Entity
public class Project extends BaseEntity {

    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_CODE = "projectCode";
    public static final String PROJECT_DOMAIN_NAME = "projectDomainName";

    // 项目名称
    @Column(unique = true)
    private String projectName;
    // 项目编码
    @Column(unique = true)
    private String projectCode;
    // 项目域名
    @Column(unique = true)
    private String projectDomainName;
}
