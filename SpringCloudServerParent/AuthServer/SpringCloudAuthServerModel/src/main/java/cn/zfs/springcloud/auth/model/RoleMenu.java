package cn.zfs.springcloud.auth.model;

import cn.zfs.springcloud.auth.BaseEntity;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;

/**
 * @ClassName: RoleMenu
 * @Author: ZFS
 * @Date: 2018/11/14 10:12
 */
@Data
@ToString(callSuper = true)
@Entity
public class RoleMenu extends BaseEntity {

    // 项目编码
    private String projectCode;
    // 角色编码
    private String roleCode;
    // 菜单编码
    private String menuCode;
}
