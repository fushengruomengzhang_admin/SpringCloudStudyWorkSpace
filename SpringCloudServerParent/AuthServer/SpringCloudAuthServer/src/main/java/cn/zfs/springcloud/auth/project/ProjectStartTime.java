package cn.zfs.springcloud.auth.project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 项目启动
 */
@Component
@Order(value = 0)
// 项目启动完成是执行,ApplicationRunner,CommandLineRunner,实现其中任何一个都success
public class ProjectStartTime implements ApplicationRunner,CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(ProjectStartTime.class);

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("项目启动----------ApplicationRunner-----------------------");
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("项目启动----------CommandLineRunner-----------------------");
    }
}
