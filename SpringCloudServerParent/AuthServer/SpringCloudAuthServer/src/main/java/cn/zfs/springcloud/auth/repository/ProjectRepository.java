package cn.zfs.springcloud.auth.repository;

import cn.zfs.springcloud.auth.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: ProjectRepository
 * @Author: ZFS
 * @Date: 2018/11/14 10:27
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project> {

}
