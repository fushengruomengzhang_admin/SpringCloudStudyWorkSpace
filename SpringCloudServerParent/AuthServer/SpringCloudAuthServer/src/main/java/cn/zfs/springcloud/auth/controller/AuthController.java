package cn.zfs.springcloud.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthController {

    @PostMapping("attr1")
    public String attrTest1(String [] a){
        System.out.println(a);
        return "success";
    }

    @PostMapping("attr2") // true 传递的时候不能带参数名的json [1,2,3,4,5]
    public String attrTest2(@RequestBody List<String> a){
        System.out.println(a);
        return "success";
    }

    @GetMapping("attr3") // true
    public String attrTest3(String [] a){
        System.out.println(a);
        return "success";
    }
}
