package cn.zfs.springcloud.auth.repository.specification;

import cn.zfs.springcloud.auth.model.Project;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import java.util.Set;

/**
 * @ClassName: ProjectSpecification
 * @Author: ZFS
 * @Date: 2018/11/14 10:36
 */
public class ProjectSpecification implements Specification<Project> {


    @Override
    public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        EntityType<Project> model = root.getModel();
        Set<Attribute<? super Project, ?>> attributes = model.getAttributes();
        for (Attribute<? super Project, ?> attribute : attributes) {
            attribute.getName();
        }
        return null;
    }
}
