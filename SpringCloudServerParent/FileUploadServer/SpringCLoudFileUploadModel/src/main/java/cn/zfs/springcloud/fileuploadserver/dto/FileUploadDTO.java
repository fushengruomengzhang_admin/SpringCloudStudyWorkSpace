package cn.zfs.springcloud.fileuploadserver.dto;

import cn.zfs.springcloud.fileuploadserver.model.FileUpload;
import lombok.Data;
import lombok.ToString;

@SuppressWarnings("Lombok")
@Data
@ToString
public class FileUploadDTO extends FileUpload {

}



