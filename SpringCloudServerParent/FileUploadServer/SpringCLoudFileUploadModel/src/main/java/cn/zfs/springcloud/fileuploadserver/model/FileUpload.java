package cn.zfs.springcloud.fileuploadserver.model;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class FileUpload {

    private String fileName;
    private String filePath;
    private String fileUrlPath;
}



