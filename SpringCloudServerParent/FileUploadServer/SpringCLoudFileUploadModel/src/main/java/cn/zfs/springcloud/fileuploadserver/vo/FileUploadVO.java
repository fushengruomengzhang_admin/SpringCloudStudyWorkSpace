package cn.zfs.springcloud.fileuploadserver.vo;

import cn.zfs.springcloud.fileuploadserver.model.FileUpload;
import lombok.Data;
import lombok.ToString;


@SuppressWarnings("Lombok")
@Data
@ToString
public class FileUploadVO extends FileUpload {
}
