package cn.zfs.springcloud.fileupload.server.hystrix;

import cn.zfs.springcloud.fileupload.server.feign.FileUploadServerFeignClient;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class FileUploadServerHystrix implements FallbackFactory<FileUploadServerFeignClient>{

    private Logger logger = LoggerFactory.getLogger(FileUploadServerHystrix.class);

    @Override
    public FileUploadServerFeignClient create(Throwable cause) {
        return new FileUploadServerFeignClient() {
            @Override
            public Map<String, Object> fileUpload(MultipartFile file) {
                logger.error("服务降级--上传文件失败");
                HashMap<String, Object> resultMap = new HashMap<>();
                resultMap.put("msg", "上传失败");
                return resultMap;
            }
        };
    }
}
