package cn.zfs.springcloud.fileupload.server.feign;

import cn.zfs.springcloud.fileupload.server.config.FileUploadLoggerConfig;
import cn.zfs.springcloud.fileupload.server.hystrix.FileUploadServerHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Administrator
 */
@FeignClient(
        name = "file-upload-server",
        fallbackFactory = FileUploadServerHystrix.class,
        configuration = {FileUploadLoggerConfig.class})
public interface FileUploadServerFeignClient {

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    Map<String, Object> fileUpload(@RequestParam(value = "file") MultipartFile file);
}
