package cn.zfs.springcloud.fileupload.server.controller;

import cn.zfs.springcloud.fileupload.server.feign.FileUploadServerFeignClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 */
@RestController
public class FileUploadController implements FileUploadServerFeignClient {

    private Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Value("${upload.file.path}")
    private String fileUploadPath;

    @Override
    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public Map<String, Object> fileUpload(@RequestParam(value = "file") MultipartFile file) {
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            byte[] bytes = file.getBytes();
            File fileToSave = new File(fileUploadPath + file.getOriginalFilename());
            FileCopyUtils.copy(bytes,fileToSave);
            resultMap.put("suceess",true);
            resultMap.put("msg","上传成功");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return resultMap;
    }
}
