package cn.zfs.springcloud.taskserver.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Task {

    private Integer id;
    private String userName;
    private String password;

    public Task() {
    }

    public Task(Integer id, String userName, String password) {
        this.id = id;
        this.userName = userName;
        this.password = password;
    }
}
