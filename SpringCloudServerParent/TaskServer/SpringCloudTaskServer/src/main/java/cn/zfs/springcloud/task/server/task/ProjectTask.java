package cn.zfs.springcloud.task.server.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ProjectTask {

    private Logger logger = LoggerFactory.getLogger(ProjectTask.class);

    @Scheduled(cron = "5 * * * * *")
    public void cron() throws Exception {
        logger.info("定时任务,当前时间:{}", new Date());
    }
}
