package cn.zfs.springcloud.task.server.feign;


import cn.zfs.springcloud.task.server.config.TaskFeignLoggerConfig;
import cn.zfs.springcloud.task.server.hystrix.TaskServerHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;

import java.util.Map;

/**
 * @author Administrator
 */
@FeignClient(
        name = "user-server",
        fallbackFactory = TaskServerHystrix.class,
        configuration = {TaskFeignLoggerConfig.class})
public interface TaskServerFeignClient {

}
