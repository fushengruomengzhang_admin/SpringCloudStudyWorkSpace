package cn.zfs.springcloud.task.server.hystrix;

import cn.zfs.springcloud.task.server.feign.TaskServerFeignClient;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 */
@Component
public class TaskServerHystrix implements FallbackFactory<TaskServerFeignClient> {

    private Logger logger = LoggerFactory.getLogger(TaskServerHystrix.class);

    @Override
    public TaskServerFeignClient create(Throwable cause) {
        logger.info("做个记号测试一下");
        return new TaskServerFeignClient() {

        };
    }
}
