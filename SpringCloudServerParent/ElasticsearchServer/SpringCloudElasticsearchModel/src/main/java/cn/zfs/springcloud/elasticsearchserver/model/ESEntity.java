package cn.zfs.springcloud.elasticsearchserver.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @ClassName: ESEntity
 * @Author: ZFS
 * @Date: 2018/9/20 16:45
 */
@Data
// indexName索引名称 可以理解为数据库名 type类型 可以理解为表名
@Document(indexName = "elasticsearch", type = "test1")
public class ESEntity {
    @Id
    private Long id;
    private String name;
    private String description;

}
