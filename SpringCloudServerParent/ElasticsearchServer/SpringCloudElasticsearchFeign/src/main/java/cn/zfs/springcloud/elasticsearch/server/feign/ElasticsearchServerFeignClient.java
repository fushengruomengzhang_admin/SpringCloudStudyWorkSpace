package cn.zfs.springcloud.elasticsearch.server.feign;


import cn.zfs.springcloud.elasticsearch.server.config.ElasticsearchFeignLoggerConfig;
import cn.zfs.springcloud.elasticsearch.server.hystrix.ElasticsearchServerHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author Administrator
 */
@FeignClient(
        name = "user-server",
        fallbackFactory = ElasticsearchServerHystrix.class,
        configuration = {ElasticsearchFeignLoggerConfig.class})
public interface ElasticsearchServerFeignClient {

}
