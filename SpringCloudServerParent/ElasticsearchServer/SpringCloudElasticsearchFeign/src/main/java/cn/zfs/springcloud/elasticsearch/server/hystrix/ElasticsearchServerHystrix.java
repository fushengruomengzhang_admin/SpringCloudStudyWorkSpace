package cn.zfs.springcloud.elasticsearch.server.hystrix;

import cn.zfs.springcloud.elasticsearch.server.feign.ElasticsearchServerFeignClient;
import cn.zfs.springcloud.util.model.BaseResponse;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class ElasticsearchServerHystrix implements FallbackFactory<ElasticsearchServerFeignClient> {

    private Logger logger = LoggerFactory.getLogger(ElasticsearchServerHystrix.class);

    @Override
    public ElasticsearchServerFeignClient create(Throwable cause) {
        logger.info("做个记号测试一下");
        return new ElasticsearchServerFeignClient() {

        };
    }
}
