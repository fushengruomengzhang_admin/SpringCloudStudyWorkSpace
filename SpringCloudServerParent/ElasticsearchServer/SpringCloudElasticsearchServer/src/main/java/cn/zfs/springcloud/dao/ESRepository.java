package cn.zfs.springcloud.dao;

import cn.zfs.springcloud.elasticsearchserver.model.ESEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ESRepository
 * @Author: ZFS
 * @Date: 2018/9/20 17:45
 */
@Component
public interface ESRepository extends ElasticsearchRepository<ESEntity, Long> {

}
