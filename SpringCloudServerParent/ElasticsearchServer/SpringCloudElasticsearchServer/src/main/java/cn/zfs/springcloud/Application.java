package cn.zfs.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.feign.EnableFeignClients;


/**
 * @author Administrator
 */
//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
/*开启feign*/
//@EnableFeignClients
/*开启eureka注解 或者使用@EnableDiscoveryClient*/
//@EnableEurekaClient
//@EnableCircuitBreaker
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
