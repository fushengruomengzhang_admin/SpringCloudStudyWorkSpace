package cn.zfs.springcloud.login.server.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Administrator
 */
public interface LoginService {
    Map<String, Object> getUserList();

    Map<String, Object> getUserByName(String userName);

    Map<String, Object> throwByZero() throws Exception;

    Map<String, Object> fileUpload(MultipartFile multipartFile);
}
