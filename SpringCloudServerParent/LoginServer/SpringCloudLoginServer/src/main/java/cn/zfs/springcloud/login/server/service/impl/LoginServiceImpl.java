package cn.zfs.springcloud.login.server.service.impl;

import cn.zfs.springcloud.fileupload.server.feign.FileUploadServerFeignClient;
import cn.zfs.springcloud.login.server.service.LoginService;
import cn.zfs.springcloud.user.server.feign.UserServerFeignClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Administrator
 */
@Service
public class LoginServiceImpl implements LoginService {

    private Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private UserServerFeignClient userServerFeignClient;
    @Autowired
    private FileUploadServerFeignClient fileUploadServerFeignClient;

    @Override
    public Map<String, Object> getUserList() {
        logger.info("登录服务--获取用户列表");
        return userServerFeignClient.getUserList();
    }

    @Override
    public Map<String, Object> getUserByName(String userName) {
        return userServerFeignClient.getUserByName(userName);
    }

    @Override
    public Map<String, Object> throwByZero() throws Exception {
        return userServerFeignClient.throwByZero();
    }

    @Override
    public Map<String, Object> fileUpload(MultipartFile multipartFile) {
        return fileUploadServerFeignClient.fileUpload(multipartFile);
    }
}
