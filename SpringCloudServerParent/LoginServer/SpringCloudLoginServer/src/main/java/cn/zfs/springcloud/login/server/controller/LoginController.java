package cn.zfs.springcloud.login.server.controller;


import cn.zfs.springcloud.login.server.feign.LoginServerFeignClient;
import cn.zfs.springcloud.login.server.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Administrator
 */
@RestController
public class LoginController implements LoginServerFeignClient {

    @Autowired
    private LoginService loginService;

    @GetMapping("/getUserList")
    @Override
    public Map<String,Object> getUserList(){
        return loginService.getUserList();
    }

    @Override
    @GetMapping("getUserByName")
    public Map<String, Object> getUserByName(@RequestParam String userName){
        return loginService.getUserByName(userName);
    }

    @Override
    @GetMapping("throwByZero")
    public Map<String, Object> throwByZero() throws Exception {
        return loginService.throwByZero();
    }

    @Override
    @PostMapping("fileUpload")
    public Map<String, Object> fileUpload(@RequestParam(value = "file") MultipartFile file) {
        return loginService.fileUpload(file);
    }
}
