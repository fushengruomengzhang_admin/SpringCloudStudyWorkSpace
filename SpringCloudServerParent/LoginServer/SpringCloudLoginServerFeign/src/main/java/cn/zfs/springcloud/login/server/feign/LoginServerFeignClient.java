package cn.zfs.springcloud.login.server.feign;

import cn.zfs.springcloud.login.server.config.LoginServerFeignLoggerConfig;
import cn.zfs.springcloud.login.server.hystrix.LoginServerHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Administrator
 */
@FeignClient(name = "login-server", fallback = LoginServerHystrix.class, configuration = {LoginServerFeignLoggerConfig.class})
public interface LoginServerFeignClient {

    @GetMapping("getUserList")
    Map<String, Object> getUserList();

    @GetMapping("getUserByName")
    Map<String, Object> getUserByName(@RequestParam String userName);

    @GetMapping("throwByZero")
    Map<String, Object> throwByZero() throws Exception;

    @PostMapping("fileUpload")
    Map<String, Object> fileUpload(@RequestParam(value = "file") MultipartFile file);
}
