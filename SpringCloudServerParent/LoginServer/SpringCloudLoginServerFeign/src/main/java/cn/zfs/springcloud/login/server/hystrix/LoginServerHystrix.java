package cn.zfs.springcloud.login.server.hystrix;

import cn.zfs.springcloud.login.server.feign.LoginServerFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Administrator
 */
@Component
public class LoginServerHystrix implements FallbackFactory<LoginServerFeignClient> {

    @Override
    public LoginServerFeignClient create(Throwable cause) {
        return new LoginServerFeignClient() {
            @Override
            public Map<String, Object> getUserList() {
                return null;
            }

            @Override
            public Map<String, Object> getUserByName(String userName) {
                return null;
            }

            @Override
            public Map<String, Object> throwByZero() throws Exception {
                return null;
            }

            @Override
            public Map<String, Object> fileUpload(MultipartFile file) {
                return null;
            }
        };
    }
}
