* 开发工具: idea
* 版本控制工具: git
* 开发环境: jdk1.8\spring cloud\lombok(需要安装lombok插件)...
# 项目结构概括
|server-name|port|domain|visit
|:---:|:---:|:---:|:---|
|register-server|9091/9092|eureka.zhangfusheng.cn|http://eureka.zhangfusheng.cn <br/> http://eureka.zhangfusheng.cn?port=9091/9092|
|config-server|9093|config.zhangfusheng.cn|http://config.zhangfusheng.cn <br/> http://config.zhangfusheng.cn/search?serverName=user-server&branch=master <br/> # serverName-->参数名称\branch-->分支|
# 端口:
|server-name|desc|start| stop|
|---|---|---|---|
|register-server-peer1| 注册中心一号| java -jar ProjectName --spring.profiles.active=peer1|http://eureka.zhangfusheng.cn/shutdown?port=9091/9092(未测试)|
|register-server-peer2| 注册中心二号|java -jar ProjectName --spring.profiles.active=peer2|http://eureka.zhangfusheng.cn/shutdown?port=9091/9092(未测试)|
|config-server|配置中心| java -jar ProjectName|http://config.zhangfusheng.cn/shutdown|
|zuul-server|网关|java -jar ProjectName|http://zuul.zhangfusheng.cn/shutdown|
|turbine-hystrix-dashboard-server|监控中心|
|user-server|用户服务|
|login-server|登录服务|
|file-upload-server|文件服务|
|auth-server|权限服务|
|task-server|定时任务服务|
# 域名描述
|domain|desc|use|use-desc|
|:---|:---|:---|:---|
|eureka.zhangfusheng.cn|注册中心|www.eureka.zhangfusheng.cn/register/server?port=9091|访问端口号为9091的注册中心|
|rabbitmq.zhangfusheng.cn| 5672端口的rabbitmq 连接使用|
|rabbitmqweb.zhangfusheng.cn| 15672端口的rabbitmq web页面访问|
|config.zhangfusheng.cn| 配置中心域名|http://config.zhangfusheng.cn/bus/refresh post请求<br/>http://config.zhangfusheng.cn/bus/refresh?destination=customers:**/9000<br/>http://config.zhangfusheng.cn/search?serverName=user-server&branch=master|刷新配置中心<br/>指定范围刷新<br/>例如访问 user-server 的 master 分支|
|turbine.zhangfusheng.cn|         

# 开发规范:  
*注:本项目结果区分为 组件服务 业务服务 插件jar包
* 包的命名规则:
```
组件服务的包命名统一为:  
    cn.
        zfs.
            springcloud
业务服务的包命名统一为: 
    cn.
        zfs.
            springcloud.
                *. (服务名,例如用户服务[user-server],星号[*]则代表user)
                    server.
                        controller
                        service.
                            impl
                         dao
插件jar包的包命名统一为:
    cn.
        zfs.
            springcloud.
                *(组件名,例如aop,星号[*]则代表aop)
                              
```
* 项目结构搭建(业务服务搭建)
```
1.  一个业务服务中包含 model feign server
2.  server 引入 feign , feign 引入 model
3.  [server[feign[model]]]
4.  server中的Controller必须实现feign中的feignClient
``` 
# 项目描述
![](http://chuantu.biz/t6/257/1521168108x-1404755660.png)
# 问题总结
```
1. 打包问题
    -- jar中没有主清单属性
    结果方案,在pom文件中引入如下内容:
        <build>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                </plugin>
            </plugins>
        </build>
2. 服务降级降不下来
    缺少配置
    feign:
      hystrix:
        enabled: true
```
## 多数据源配置
注:多数据源配置的案例在UserServer中,缺陷,暂时无法通过在dao层使用注解的方式指定数据源,只能在service层通过注解方式指定.
```
1.pom文件中需要引入如下配置
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>1.3.1</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
    </dependency>
2.配置文件中配置如下,暂时配置两个数据源:
    db_master:
      url: jdbc:mysql://10.4.82.34:3306/user_server?useUnicode=true&amp;characterEncoding=UTF-8
      username: root
      password: zfs199528
      driver-class-name: com.mysql.jdbc.Driver
    db_dev:
      url: jdbc:mysql://10.4.82.34:3306/server_user?useUnicode=true&amp;characterEncoding=UTF-8
      username: root
      password: zfs199528
      driver-class-name: com.mysql.jdbc.Driver
3.具体实现参考 cn.zfs.springcloud.user.server.datasource
```
## 注册中心测试:
```
1. 测试 
      服务器
         配置nginx eureka.zhangfusheng.cn 映射注册中心端口9091 9092
         配置hosts ip eureka.zhangfusheng.cn
         启动注册中心,观察注册中心是否可以相互注册
    测试结果:
        服务器中nginx进行如下配置
            upstream eurekaServer{
                server ip:port1;
                server ip:port2;
            }
            server {
                listen       80;
                server_name www.eureka.zhangfusheng.cn;
                #charset koi8-r;
            
                #access_log  logs/host.access.log  main;
            
                location / {
                    proxy_pass  http://eurekaServer;
                }
                error_page   500 502 503 504  /50x.html;
                location = /50x.html {
                    root   html;
                }
            }
        host文件如下配置
            ip eureka.zhangfusheng.cn
         可以实现注册中心项目注册
     不足:(其他服务是否可以通过域名注册未测试)
     可以实现注册,但是很不稳定,建议添加端口

```