#!/bin/bash
# rm -rf
echo "是否需要删除SpringCloudStudyWorkSpace?y/n"
read yn
if [ $yn = 'y' ];then
    rm -rf SpringCloudStudyWorkSpace
fi

# clone
echo "是否需要重新克隆仓库?y/n"
read yn
if [ $yn = 'y' ];then
    git clone git@gitee.com:fushengruomengzhang_admin/SpringCloudStudyWorkSpace.git
fi
# cd SpringCloudStudyWorkSpace and mvn clean install
cd SpringCloudStudyWorkSpace
echo "是否需要打包?y/n"
read yn
if [ $yn = 'y' ];then
    mvn clean install -Dmaven.test.skip=true
fi
# cp *.jar to jar
path=`pwd`/SpringCloudAssembly/
echo "是否需要复制jar包?y/n"
read yn
if [ $yn = 'y' ];then
    declare -a list_result=(`find "$path" -name *.jar`)
    for result in ${list_result[@]}
    do
        for key in ${!result[@]}
        do
            echo ${result[$key]}
            rm -rf jar/${result[$key]}
            cp -rf ${result[$key]} ../jar/
        done
    done
fi
# start and shutdown
declare -a list_result_start=(
"SpringCloudRegisterServer-1.0-SNAPSHOT.jar --spring.profiles.active=peer1"
"SpringCloudRegisterServer-1.0-SNAPSHOT.jar --spring.profiles.active=peer2"
"SpringCloudConfigServer-1.0-SNAPSHOT.jar"
"SpringCloudZuulServer-1.0-SNAPSHOT.jar"
"SpringCloudTurbineHystrixServer-1.0-SNAPSHOT.jar"
)
declare -a list_result_server_name=(
"register-server-1"
"register-server-2"
"config-server"
"zull-server"
"turbine-server"
)
declare -a list_result_shutdown=(
"http://eureka.zhangfusheng.cn/shutdown?port=9091"
"http://eureka.zhangfusheng.cn/shutdown?port=9092"
"http://config.zhangfusheng.cn/shutdown"
"http://zuul.zhangfusheng.cn/shutdown"
"http://turbine.zhangfusheng.cn/shutdown"
)
declare -a list_result_info=(
"http://eureka.zhangfusheng.cn/info?port=9091"
"http://eureka.zhangfusheng.cn/info?port=9092"
"http://config.zhangfusheng.cn/info"
"http://zuul.zhangfusheng.cn/info"
"http://turbine.zhangfusheng.cn/info"
)
cd ../jar
for i in ${!list_result_start[@]}
do
    echo "********************************************************************************************************"
    echo curl -u name:password -X POST -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_shutdown[$i]}
    echo `curl -u name:password -X POST -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_shutdown[$i]}`
    java -jar ${list_result_start[$i]} > "../log/${list_result_server_name[$i]}"-log.log 2>&1 &
    while ((1))
    do
        sleep 2s
        echo curl -u name:password -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_info[$i]}
        statu=`curl -u name:password -I -m 10 -o /dev/null -s -w %{http_code} ${list_result_info[$i]}`
        echo $statu
        if (($statu == 200));then
            break
        fi
    done
    echo "********************************************************************************************************"
done
